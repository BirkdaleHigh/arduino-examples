# Arduino Examples

References and longer explanation than the hardware-club-start project.

## Adding Complexity

See the `README.md` text file in that folder for specifics. This is an example of how code changes as you try to add to it from the initial point of getting something working.
