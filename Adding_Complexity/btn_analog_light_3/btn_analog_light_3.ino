int pin_btn = 13;
int pin_led =  3;
int pin_dial = A1;

void setup() {
  // put your setup code here, to run once:
  pinMode(pin_btn, INPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_dial, INPUT);
  Serial.begin(115200);
}

bool toggle = 0;
bool btn_hold = 0;
void check_btn(){
  if (digitalRead(pin_btn) == 1) { // true is pressed
    if(btn_hold == 0){ // only invert toggle once if the button is held down
      toggle = !toggle;
    }
    btn_hold = 1;
    Serial.println("button pressed");
  } else {
    btn_hold = 0;
    Serial.println("button not pressed");
  }
}

void set_brightness(){
  int dial = analogRead(pin_dial);
  int amount = map(dial, 0,1024,0,255);
  analogWrite(pin_led, amount);
  Serial.println(dial);
}

void loop() {
  // put your main code here, to run repeatedly:
  check_btn();

  if(toggle) {
    Serial.print("button toggled on. Brightness: " );
    set_brightness();
  } else {
    analogWrite(pin_led, 0);
    Serial.println("toggle off");
  }

  delay(300);
}
